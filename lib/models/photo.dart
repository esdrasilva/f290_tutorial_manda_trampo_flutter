class Photo {
  String id;
  String serviceId;
  String url;
  String localUrl;

  Photo({this.id, this.serviceId, this.url, this.localUrl});

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      id: json['id'],
      serviceId: json['services_id'],
      url: json['url'],
      localUrl: json['local_url']
    );
  }
}