import 'package:tutorial_manda_trampo/models/category.dart';
import 'package:tutorial_manda_trampo/models/photo.dart';
import 'package:tutorial_manda_trampo/models/user.dart';

class Service {
  String id = '';
  String name = '';
  String description = '';
  String address = '';
  String city = '';
  String state = '';
  String phone = '';
  String cellphone = '';
  String email = '';
  String site = '';
  String linkFacebook = '';
  String linkInstagram = '';
  String openingHours = '';
  String aproved = '';
  int denunciation;
  bool isWhats;
  String categoriesId = '';
  User user;
  ServiceCategory category;
  List<Photo> photos;

  Service(
      {this.id,
      this.name,
      this.description,
      this.address,
      this.city,
      this.state,
      this.phone,
      this.cellphone,
      this.email,
      this.site,
      this.linkFacebook,
      this.linkInstagram,
      this.openingHours,
      this.aproved,
      this.denunciation,
      this.isWhats,
      this.categoriesId,
      this.user,
      this.category,
      this.photos});

  Service.fromJson(Map<String, dynamic> json) {    
      this.id = json['id'];
      this.name = json['name'];
      this.description = json['description'];
      this.address = json['address'];
      this.city = json['city'];
      this.state = json['state'];
      this.phone = json['phone'];
      this.cellphone = json['celphone'];
      this.email = json['email'];
      this.site = json['site'];
      this.linkFacebook = json['link_facebook'];
      this.linkInstagram = json['link_instagram'];
      this.openingHours = json['opening_ours'];
      this.aproved = json['aproved'];
      this.denunciation = json['denunciation'];
      this.isWhats = json['iswhats'];
      this.categoriesId = json['categories_id'];

      this.user = User.fromJson(json['user']);

      this.category = ServiceCategory.fromJson(json['categories']);

      List photoList = json['photo'];

      List<Photo> photos = List();

      photoList.forEach((element) {
        photos.add(Photo.fromJson(element));
      });
      
      this.photos = photos;  
  }
}
