import 'package:tutorial_manda_trampo/models/user.dart';

class Curriculum {
  String id;
  String userId;
  String curriculum;
  String linkMediaSocial;
  String description;
  String professionId;
  String professionOthers;
  String experienceTime;
  User user;
  Profession profession;

  Curriculum.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.userId = json['user_id'];
    this.curriculum = json['value'];
    this.linkMediaSocial = json['curriculum'];
    this.description = json['description'];
    this.professionId = json['profession_id'];
    this.professionOthers = json['profession_others'];
    this.experienceTime = json['experience_time'];
    this.user = User.fromJson(json['user']);
    this.profession = Profession.fromJson(json['professions']);    
  }
}

class Profession {
  String id;
  String name;

  Profession.fromJson(Map<String, dynamic> json)
      : this.id = json['id'],
        this.name = json['name'];
}
