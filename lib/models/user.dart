class User {
  String id;
  String name;
  String email;
  String avatar;
  String phone;
  String cellPhone;
  String address;
  String city;
  String country;
  String state;
  String isWhats;
  String level;
  String avatarUrl;

  User(
      {this.id,
      this.name,
      this.email,
      this.avatar,
      this.phone,
      this.cellPhone,
      this.address,
      this.city,
      this.country,
      this.state,
      this.isWhats,
      this.level,
      this.avatarUrl});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    avatar = json['avatar'];
    phone = json['phone'];
    cellPhone = json['celphone'];
    address = json['address'];
    city = json['city'];
    country = json['country'];
    state = json['data'];
    isWhats = json['state'];
    level = json['level'];
    avatarUrl = json['avatar_url'];
  }
}
