class ServiceCategory {
  String id;
  String name;

  ServiceCategory({this.id, this.name});

  factory ServiceCategory.fromJson(Map<String, dynamic> json) {
    return ServiceCategory(id: json['id'], name: json['name']);
  }
}
