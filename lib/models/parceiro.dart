class Parceiro {
  String parceiro;
  String endereco;
  String cidade;
  String telefone;
  String url;
  String horario;
  String whatsApp;
  String instagram;
  String imagem;

  Parceiro({this.parceiro, this.imagem, this.endereco, this.cidade,
  this.telefone, this.url, this.horario, this.whatsApp, this.instagram});

  Map<String, dynamic> toMap() {
    var map = {
      'parceiro': parceiro,
      'endereco': endereco,
      'cidade': cidade,
      'telefone': telefone,
      'url': url,
      'horario': horario,
      'whatsApp': whatsApp,
      'instagram': instagram,
      'imagem': imagem
    };
    return map;
  }
}
