import 'package:flutter/material.dart';

class SobrePage extends StatelessWidget {
  final p1 =
      'Cadastre seu currículo ou serviço na MandaTrampo. Apareça! Divulgue-se! Permita que as empresas te encontrem. Nós queremos te ajudar nesta pandemia. Plataforma gratuita, criada por alunos da FATEC Araras do curso Sistemas para Internet. ';
  final p2 =
      'MandaTrampo foi criada para você que perdeu seu emprego nesta pandemia. Cadastre gratuitamente seu currículo ou seu serviço ou um pequeno negócio. Apareça! Divulgue-se! Permita que as empresas te encontrem! ';
  final p3 =
      'MandaTrampo plataforma gratuita para ajudar as pessoas que perderam seus empregos nesta pandemia a se recolocarem e a se reinventarem. Você pode cadastrar seu currículo ou se tiver um pequeno negócio, pode divulgá-lo também. Apareça! Faça com que as pessoas e as empresas te encontrem. ';
  final p4 =
      'MandaTrampo plataforma gratuita, criada por universitários da FATEC Araras, onde você pode cadastrar seu currículo ou divulgar seu pequeno negócio. A ideia é ajudar as pessoas que perderam o emprego nesta pandemia a se recolocarem ou mostrarem seus serviços. Promover o encontro de quem procura com quem oferece.  ';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Image.asset(
            'images/mt.png',
            fit: BoxFit.cover,
          ),
          _paragrafoWidget(p1),
          _paragrafoWidget(p2),
          _paragrafoWidget(p3),
          _paragrafoWidget(p4),
        ],
      ),
    ));
  }

  _paragrafoWidget(String paragrafo) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Text(
        paragrafo,
        style: TextStyle(fontSize: 18),
      ),
    );
  }
}
