import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tutorial_manda_trampo/models/service.dart';
import 'package:tutorial_manda_trampo/widgets/parceiro_tile.dart';
import 'package:tutorial_manda_trampo/widgets/parceiro_tile_detalhe.dart';
import 'package:url_launcher/url_launcher.dart';

class ServicosDetalhe extends StatefulWidget {
  final Service service;

  ServicosDetalhe({this.service});

  @override
  _ServicosDetalheState createState() => _ServicosDetalheState();
}

class _ServicosDetalheState extends State<ServicosDetalhe> {
  List<String> imageLinks;

  void getImages() {
    imageLinks = List();
    if (widget.service.photos.isEmpty) {
      imageLinks.add('');
    } else {
      widget.service.photos.forEach((photo) {
        imageLinks.add(photo.localUrl);
      });
    }
  }

  @override
  void initState() {
    getImages();
    super.initState();
  }

  int _current = 0;

  Future<void> _abrirWebViewComJS(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Erro ao abrir $url';
    }
  }

  Future<void> _fazerLigacao(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Erro ao lançar $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.service.name}'),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CarouselSlider(
            options: CarouselOptions(
                enlargeCenterPage: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 5),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                height: 250.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
            items: imageLinks.map((url) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: url.isNotEmpty
                            ? NetworkImage(url)
                            : AssetImage('images/mt.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imageLinks.map((url) {
              int index = imageLinks.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Color(0xFF746EFF)
                      : Color.fromRGBO(0, 0, 0, 0.3),
                ),
              );
            }).toList(),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(widget.service.name, style: TextStyle(fontSize: 36)),
                  ParceiroTile(
                    icon: FontAwesomeIcons.userAlt,
                    descricao: widget.service.name,
                  ),
                  ParceiroTileComDetalhe(
                    icon: FontAwesomeIcons.handsHelping,
                    descricao: 'Descrição',
                    detalheDescricao: widget.service.description,
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.mapMarker,
                    descricao: widget.service.address ?? 'Não informado',
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.phone,
                    descricao: widget.service.phone ?? 'Não informado',
                    callBack: () {
                      if (widget.service.site.isNotEmpty) {
                        _fazerLigacao('tel:${widget.service.phone}');
                      }
                    },
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.mobile,
                    descricao: widget.service.cellphone ?? 'Não informado',
                    callBack: () {
                      if (widget.service.site.isNotEmpty) {
                        _fazerLigacao('tel:${widget.service.cellphone}');
                      }
                    },
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.link,
                    descricao: widget.service.site ?? 'Não informado',
                    callBack: () {
                      var link = widget.service.site;
                      if (link.isNotEmpty) {
                        if (link.startsWith('http')) {
                          _abrirWebViewComJS(widget.service.site);
                        }
                        if (link.startsWith('www')) {
                          _abrirWebViewComJS('http://${widget.service.site}');
                        }
                      }
                    },
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.userClock,
                    descricao: widget.service.openingHours ?? 'Não informado',
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.whatsapp,
                    descricao: widget.service.cellphone ?? 'Não informado',
                    callBack: () {
                      if (widget.service.isWhats &&
                          widget.service.cellphone.isNotEmpty) {
                        _fazerLigacao(
                            'whatsapp://send?phone=+55${widget.service.cellphone}');
                      }
                    },
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.facebook,
                    descricao: widget.service.linkFacebook ?? 'Não informado',
                    callBack: () {
                      var link = widget.service.linkFacebook;
                      if (link.isNotEmpty) {
                        if (link.startsWith('http')) {
                          _abrirWebViewComJS(widget.service.linkFacebook);
                        }
                        if (link.startsWith('/')) {
                          _abrirWebViewComJS(
                              'http://www.facebook.com${widget.service.linkFacebook}');
                        }
                      }
                    },
                  ),
                  ParceiroTile(
                    icon: FontAwesomeIcons.instagram,
                    descricao: widget.service.linkInstagram ?? 'Não informado',
                    callBack: () {
                      var link = widget.service.linkInstagram;
                      if (link.isNotEmpty) {
                        if (link.startsWith('http')) {
                          _abrirWebViewComJS(widget.service.linkInstagram);
                        }
                        if (link.startsWith('/')) {
                          _abrirWebViewComJS(
                              'https://www.instagram.com${widget.service.linkInstagram}');
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
