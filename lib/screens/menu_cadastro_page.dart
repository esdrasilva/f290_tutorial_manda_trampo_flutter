import 'package:flutter/material.dart';
import 'package:tutorial_manda_trampo/screens/home_page.dart';

class MenuCadastro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          // Centralzar widgets
          mainAxisAlignment: MainAxisAlignment.center,
          // Utilizar o tamano máximo possívem em tela.
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            //Texto simples
            Text(
              'ESCOLHA UMA DA OPÇÕES',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24),
            ),
            //Texto com configurações de margem dentro e um container
            Container(
              margin: EdgeInsets.only(left: 48, right: 48, top: 16),
              child: Text(
                'Não se preocupe, você poderá cadastrar os dois depois =)',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
            ),
            SizedBox(
              height: 64,
            ),
            RaisedButton(
              //Configuração para cantos arredondados
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32)),
              child: Text('CADASTRAR CV'),
              onPressed: () {
                // Navegação a ser adicionada
              },
            ),
            SizedBox(
              height: 24,
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32)),
              child: Text('CADASTRAR SERVIÇO/NEGÓCIO'),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
              },
            ),
          ],
        ),
      ),
    );
  }
}