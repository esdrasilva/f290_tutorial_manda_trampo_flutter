import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:tutorial_manda_trampo/models/curriculum.dart';
import 'package:http/http.dart' as http;
import 'package:tutorial_manda_trampo/widgets/parceiro_card.dart';

class CurriculosPage extends StatefulWidget {
  @override
  _CurriculosPageState createState() => _CurriculosPageState();
}

class _CurriculosPageState extends State<CurriculosPage> {
  List<Curriculum> curriculuns;

  @override
  void initState() {
    _getCurriculuns();
    super.initState();
  }

  _getCurriculuns() async {
    http.Response response =
        await http.get('http://mandatrampo.com.br:3333/curriculum');
    List listCurriculuns = jsonDecode(response.body);
    curriculuns = listCurriculuns.map((c) => Curriculum.fromJson(c)).toList();
    print(curriculuns.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _getCurriculuns(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Container(
                alignment: Alignment.center,
                child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 200,
                  color: Theme.of(context).primaryColor,
                ),
              );
            default:
              return Container(
                child: _criarListaCardSCurriculo(context),
              );
          }
        },
      ),
    );
  }

  Widget _criarListaCardSCurriculo(BuildContext context) {
    return ListView.builder(
      itemCount: curriculuns.length,
      itemBuilder: (context, index) {
        return ParceiroCard(
          curriculum: curriculuns[index],
          cidade: curriculuns[index].user.city ?? 'Não informado',
          endereco: curriculuns[index].user.name ?? 'Não informado',
          parceiro: curriculuns[index].profession.name ?? 'Não informado',
          experiencia: curriculuns[index].experienceTime ?? 'Não informado',
          urlImagem: curriculuns[index].user.avatarUrl,
        );
      },
    );
  }
}
