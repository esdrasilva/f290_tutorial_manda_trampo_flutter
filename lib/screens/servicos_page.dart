import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:tutorial_manda_trampo/models/service.dart';
import 'package:tutorial_manda_trampo/widgets/card_servico.dart';
import 'package:http/http.dart' as http;

class ServicosPage extends StatefulWidget {
  @override
  _ServicosPageState createState() => _ServicosPageState();
}

class _ServicosPageState extends State<ServicosPage> {
  List<Service> services;

  @override
  void initState() {
    _getServices();
    super.initState();
  }

  _getServices() async {
    http.Response response =
        await http.get('http://www.mandatrampo.com.br:3333/services');
    List listServices = jsonDecode(response.body);
    services = listServices.map((e) => Service.fromJson(e)).toList();
    print(services.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _getServices(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Container(
                alignment: Alignment.center,
                child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 200,
                  color: Theme.of(context).primaryColor,
                ),
              );
            default:
              return Container(
                child: _criarListaCardServicoNovo(context),
              );
          }
        },
      ),
    );
  }

  Widget _criarListaCardServicoNovo(BuildContext context) {
    return ListView.builder(
      itemCount: services.length,
      itemBuilder: (context, index) {
        return CardSimpleService(
          service: services[index],
        );
      },
    );
  }
}
