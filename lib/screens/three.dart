import 'package:flutter/material.dart';
import 'package:tutorial_manda_trampo/models/curriculum.dart';

class Three extends StatelessWidget {
  final Curriculum currriculum;
  const Three({this.currriculum});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 250,
            pinned: true,
            centerTitle: true,
            elevation: 2,
            flexibleSpace: FlexibleSpaceBar(
                title: Text(currriculum.user.name.split(' ').first),
                background: Stack(
                  children: [
                    currriculum.user.avatarUrl.isEmpty
                        ? Container(
                            child: Image.asset(
                              'images/mt.png',
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      NetworkImage(currriculum.user.avatarUrl),
                                  fit: BoxFit.cover),
                            ),
                          ),
                    Container(
                      // height: 300.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        gradient: LinearGradient(
                          begin: FractionalOffset.topCenter,
                          end: FractionalOffset.bottomCenter,
                          colors: [
                            Colors.black.withOpacity(0.0),
                            Colors.black,
                          ],
                          stops: [0.33, 1.0],
                        ),
                      ),
                    ),
                    Positioned(
                        bottom: 16, left: 16, right: 0, child: Container()),
                  ],
                )),
          ),
          SliverFillRemaining(
            child: SafeArea(
              child: Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _buildList(7),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  List _buildList(int count) {
    List<Widget> listItems = List();

    var map = {
      'Nome': currriculum.user.name,
      'Cargo': currriculum.profession.name,
      'E-mail': currriculum.user.email,
      'Cidade': currriculum.user.city,
      'Experiência': currriculum.experienceTime,
      'Telefone': currriculum.user.phone,
      'Descrição': currriculum.description
    };

    map.forEach((key, value) {
      listItems.add(
        Padding(
            padding: EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                style: TextStyle(color: Colors.black, fontSize: 22),
                children: <TextSpan>[
                  TextSpan(
                      text: '$key ',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  TextSpan(text: '$value '),
                ],
              ),
            )),
      );
    });

    return listItems;
  }
}
