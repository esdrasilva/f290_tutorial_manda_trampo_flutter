import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tutorial_manda_trampo/screens/servicos_page.dart';
import 'package:tutorial_manda_trampo/screens/curriculos_page.dart';
import 'package:tutorial_manda_trampo/screens/sobre.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _paginas = List<Widget>();

  var paginaSelecionada = 0;

  @override
  void initState() {
    _paginas.add(ServicosPage());
    _paginas.add(CurriculosPage());
    _paginas.add(SobrePage());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manda Trampo'),
      ),
      drawer: SafeArea(
        child: Drawer(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                accountName: Text('MandaTrampo'),
                accountEmail: Text('mandatrampo.com.br'),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  backgroundImage: AssetImage(
                    'images/mt.png',
                  ),
                ),
              ),
              ListTile(
                leading: Icon(FontAwesomeIcons.envelope),
                title: Text('Fale conosco'),
                subtitle: Text('Envie um e-mail pra gente...'),
                trailing: Icon(Icons.arrow_forward),
              ),
            ],
          ),
        ),
      ),
      body: IndexedStack(
        index: paginaSelecionada,
        children: _paginas,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: paginaSelecionada,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Serviços'),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.userAlt),
            title: Text(
              'CV',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              FontAwesomeIcons.users,
            ),
            title: Text(
              'Sobre nós',
            ),
          ),
        ],
        onTap: (index) {
          setState(() {
            paginaSelecionada = index;
          });
        },
      ),
    );
  }
}
