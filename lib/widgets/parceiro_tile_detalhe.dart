import 'package:flutter/material.dart';

class ParceiroTileComDetalhe extends StatelessWidget {
  final IconData icon;
  final String descricao;
  final String detalheDescricao;
  final Function callBack;
  final double tamanhoIcone;
  final double tamanhoFonte;

  ParceiroTileComDetalhe(
      {this.icon,
      this.descricao,
      this.detalheDescricao,
      this.callBack,
      this.tamanhoIcone,
      this.tamanhoFonte});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        color: Theme.of(context).accentColor,
        size: tamanhoIcone,
      ),
      title: Text(
        descricao,
        style: TextStyle(fontSize: tamanhoFonte),
      ),
      subtitle: Text(detalheDescricao),
      onTap: callBack,
    );
  }
}
