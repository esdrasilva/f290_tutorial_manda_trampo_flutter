import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tutorial_manda_trampo/models/curriculum.dart';
import 'package:tutorial_manda_trampo/screens/three.dart';

class ParceiroCard extends StatelessWidget {
  final double tamanhoIcone;
  final double tamanhoFonte;
  final String parceiro;
  final String endereco;
  final String cidade;
  final String telefone;
  final String urlImagem;
  final String experiencia;
  final Curriculum curriculum;

  const ParceiroCard(
      {Key key,
      this.tamanhoIcone,
      this.tamanhoFonte,
      this.parceiro,
      this.endereco,
      this.cidade,
      this.telefone,
      this.urlImagem,
      this.experiencia,
      this.curriculum});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        semanticContainer: true,
        margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Container(
              height: 220,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage('images/fundo.png'),
                      fit: BoxFit.cover)),
            ),
            Column(
              children: [
                SizedBox(
                  height: 16,
                ),
                Center(
                  child: Container(
                    width: 50,
                    height: 15,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('images/furo_cartao_2.png')),
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.all(16),
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(4),
                            border: Border.all(
                                color: Theme.of(context).accentColor, width: 2),
                            color: Theme.of(context).accentColor,
                            image: DecorationImage(
                              image: urlImagem.isEmpty
                                  ? AssetImage('images/mt.png')
                                  : NetworkImage(urlImagem),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Image.asset(
                          'images/mt.png',
                          height: 50,
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                    Expanded(
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: Text(
                              parceiro,
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                                child: Icon(
                                  FontAwesomeIcons.user,
                                  size: 20,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Container(
                                  width: 160,
                                  child: Text(
                                    endereco,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                                child: Icon(
                                  FontAwesomeIcons.globe,
                                  size: 20,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  cidade,
                                  style: TextStyle(fontSize: 12),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                                child: Icon(
                                  FontAwesomeIcons.trophy,
                                  size: 20,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  experiencia,
                                  style: TextStyle(fontSize: 12),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          Center(
                            child: MaterialButton(
                              color: Colors.white,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Three(
                                              currriculum: curriculum,
                                            )));
                              },
                              splashColor: Colors.redAccent.withAlpha(30),
                              shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(4.0),
                                borderSide: BorderSide(
                                  color: Colors.deepPurpleAccent,
                                  width: 1,
                                ),
                              ),
                              child: Text(
                                'Veja Mais',
                                style: TextStyle(color: Colors.deepPurple),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
