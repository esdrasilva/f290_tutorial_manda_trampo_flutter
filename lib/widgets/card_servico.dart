import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tutorial_manda_trampo/models/service.dart';
import 'package:tutorial_manda_trampo/screens/servicos_detalhe.dart';

class CardSimpleService extends StatelessWidget {
  final Service service;

  CardSimpleService({this.service});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: InkWell(
        splashColor: Colors.blue.withAlpha(30),
        onTap: () {
          print("Click!");
        },
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 200,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: service.photos.isEmpty
                          ? AssetImage('images/mt.png')
                          : NetworkImage(service.photos[0].localUrl),
                      fit: BoxFit.fitWidth),
                ),
              ),
              ListTile(
                // leading: Icon(Icons.add_a_photo),
                title: Text(
                  service.name,
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: RichText(
                          text: TextSpan(
                              text: 'Nome: ',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              children: [
                            TextSpan(
                                text: service.user.name ?? 'Não informado =(',
                                style: TextStyle(fontWeight: FontWeight.normal))
                          ])),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: RichText(
                          text: TextSpan(
                              text: 'Cidade: ',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              children: [
                            TextSpan(
                                text: service.city.isNotEmpty
                                    ? '${service.city}/${service.state}'
                                    : 'Não informado',
                                style: TextStyle(fontWeight: FontWeight.normal))
                          ])),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: RichText(
                          text: TextSpan(
                              text: 'Telefone: ',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              children: [
                            TextSpan(
                                text: service.phone ?? ' Não informado =(',
                                style: TextStyle(fontWeight: FontWeight.normal))
                          ])),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                      icon: Icon(FontAwesomeIcons.facebook),
                      onPressed: () {
                        print('Click!');
                      }),
                  IconButton(
                      icon: Icon(FontAwesomeIcons.instagram),
                      onPressed: () {
                        print('Click!');
                      }),
                  IconButton(
                      icon: Icon(FontAwesomeIcons.whatsapp),
                      onPressed: () {
                        print('Click!');
                      }),
                ],
              ),
              MaterialButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ServicosDetalhe(
                                service: this.service,
                              )));
                },
                splashColor: Colors.redAccent.withAlpha(30),
                shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4.0),
                    borderSide: BorderSide(
                      color: Colors.deepPurpleAccent,
                      width: 1,
                    )),
                child: Text(
                  'Veja Mais',
                  style: TextStyle(color: Colors.deepPurple),
                ),
              ),
              SizedBox(
                height: 16,
              )
            ],
          ),
        ),
      ),
    );
  }
}
