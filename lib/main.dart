import 'package:flutter/material.dart';
import 'package:tutorial_manda_trampo/screens/home_page.dart';

main() => runApp(MandaTrampo());

class MandaTrampo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //Altera as cores dos componentes para o Tema Escuro
        // brightness: Brightness.dark,
        primaryColor: Color(0xFF746EFF),
        accentColor: Color(0xFF746EFF),
        // Cor de fundo do Scaffold
        // scaffoldBackgroundColor: Color(0xFF272727),
        scaffoldBackgroundColor: Color(0xFFEEEEEE),
        // Configuração de estilo do texto por categoria
        // textTheme: TextTheme(
        //   bodyText2: TextStyle(color: Color(0xFF7D7B85)),
        // ),
        // Cor padrão dos botões
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xFF746EFF),
        ),
        // Decoração das caixas de texto com bordas arredondadas
        inputDecorationTheme: InputDecorationTheme(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF7D7B85),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF7D7B85),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
      home: HomePage(),
    );
  }
}
